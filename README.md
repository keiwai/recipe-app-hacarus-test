# A Recipe App #

This a test app for pulling API recipes from https://developer.edamam.com
using React.JS, react-bootstrap, react-material-ui

# Note: #

	Edamam API sometimes doesn't return results on certain query.
	see: 
		https://api.edamam.com/search?q=test&from=60&app_id=3f13abcb&app_key=0b27f41ab0ab44e2630e838d2d0a8014 (results return 2000 count yet no hits are shown)
	
	The app uses no app_key and app_id in request URL as both ways are accessible only difference is using it without access keys renders the 'ingredients' in a more elaborate way.
	see:
		https://api.edamam.com/search?q=test&app_id=3f13abcb&app_key=0b27f41ab0ab44e2630e838d2d0a8014 (with private access keys)
		https://api.edamam.com/search?q=test (without private access keys)
