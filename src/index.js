import React from 'react';
import ReactDOM from 'react-dom';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import registerServiceWorker from './registerServiceWorker';

import './App.css';
import './bootstrap.min.css';
import './bootstrap-theme.min.css';
import App from './App';

const WrapApp = () => (
  <MuiThemeProvider>
    <App />
  </MuiThemeProvider>
);

ReactDOM.render(<WrapApp />, document.getElementById('root'));
registerServiceWorker();