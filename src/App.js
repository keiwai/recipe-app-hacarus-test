import React, {Component} from 'react';

// Bootstrap Components
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import ListGroup from 'react-bootstrap/lib/ListGroup';
import ListGroupItem from 'react-bootstrap/lib/ListGroupItem';
import Table from 'react-bootstrap/lib/Table';
import {Tabs, Tab} from 'material-ui/Tabs';

// Meterial UI Components
import AppBar from 'material-ui/AppBar';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress';

import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';

// Cards Usage
import Subheader from 'material-ui/Subheader';
import {Card, CardTitle, CardHeader, CardMedia } from 'material-ui/Card';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Chip from 'material-ui/Chip';

import FontIcon from 'material-ui/FontIcon';
import {grey50} from 'material-ui/styles/colors';

// Material UI TapEvent Fix
var injectTapEventPlugin = require("react-tap-event-plugin");
injectTapEventPlugin();

// Options
const paginateItems=12; // number of items per page
const appid = '3f13abcb';
const appkey = '0b27f41ab0ab44e2630e838d2d0a8014';

// with app key request
  // const baseurl = 'https://api.edamam.com/search?'+'app_id='+appid+'&app_key='+appkey+'&';
// without app key request 
  const baseurl = 'https://api.edamam.com/search?'; 

class BuildRecipeDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 'a',
    };
    this.handleSearchAction = this.handleSearchAction.bind(this);
  }

  handleChange = (value) => {
    this.setState({
      value: value,
    });
  };

  handleSearchAction(searchText) {
    console.log("BuildRecipeDialog::handleSearchAction v:"+searchText);
    if(searchText)
      this.props.updateSearchAction(searchText);
  }

  render() {
    var ingredients = [];
    var ingredientsTable = [];
    var ingqty = '';
    var ingmea = '';
    this.props.datasrc.ingredients.forEach((ingredient,index) => {
      ingredients.push(<Chip onTouchTap={ this.handleSearchAction.bind(null, ingredient.food) } className="ingredient-chip" key={index} value={ingredient.food}>{ingredient.food}</Chip>);
      ingqty = (ingredient.quantity)?ingredient.quantity:'-'
      ingmea = (ingredient.measure)?ingredient.measure:''
      ingredientsTable.push(<tr key={index}><td>{ingredient.food}</td><td>{ingqty} {ingmea}</td></tr>);
    });

    var ingredientlines = [];
    this.props.datasrc.ingredientLines.forEach((ingredient,index) => {
      ingredientlines.push(<ListGroupItem key={index}>{ingredient}</ListGroupItem>);
    });

    var totalnutrients = [];
    Object.entries(this.props.datasrc.totalNutrients).forEach((nutrient,index) => {
      totalnutrients.push(<tr key={index}><td>{nutrient[1].label}</td><td>{nutrient[1].quantity.toFixed(2)} {nutrient[1].unit}</td></tr>);
    });

    var dailynutrients = [];
    Object.entries(this.props.datasrc.totalDaily).forEach((nutrient,index) => {
      dailynutrients.push(<tr key={index}><td>{nutrient[1].label}</td><td>{nutrient[1].quantity.toFixed(2)} {nutrient[1].unit}</td></tr>);
    });

    var healthLabels = [];
    this.props.datasrc.healthLabels.forEach((healthlabel,index) => {
      healthLabels.push(<Chip onTouchTap={ this.handleSearchAction.bind(null, healthlabel) } className="ingredient-chip" key={index} value={healthlabel}>{healthlabel}</Chip>);
    });

    var divStyle = {
      backgroundImage: 'url(' + this.props.datasrc.image + ')'
    } 

    return (
      <div className="recipe-dialog-content">
        <div className="clear20"></div>

        <Row>
          <Col sm={6}> 
            <Paper zDepth={2}>
              <Col sm={12} className="recipe-dialog-content-image" style={ divStyle } > </Col>
              <div className="clear"></div>
            </Paper>
            <div className="clear10"></div>
          </Col>
          <Col sm={6}> 
            <Col sm={12} className="nopadding">
              <h4 className="recipe-dialog-heading">Ingredients</h4>
              <Divider />
              <ListGroup className="recipe-dialog-list">
                { ingredientlines }
              </ListGroup>
            </Col>
          </Col>
          <div className="clear10"></div>
        </Row>

        <Row>
          <Col sm={6}> 
            <div className="clear10"></div>
            <Col sm={12} className="nopadding"> <h5>Ingredient Tags</h5> </Col>
            <Col sm={12} className="nopadding"> { ingredients } </Col>
          </Col>
          <Col sm={6}> 
            <div className="clear10"></div>
            <Col sm={12} className="nopadding"> <h5>Health Labels</h5> </Col>
            <Col sm={12} className="nopadding"> { healthLabels } </Col>
          </Col>
          <div className="clear20"></div>
        </Row>

        <Row>
          <Tabs
            className="recipe-tabs"
            value={this.state.value}
            onChange={this.handleChange}
          >
            <Tab className="recipe-tab" label="Nutrition Value" value="a">
              <Col sm={12} className="nopadding">
                <Table className="recipe-dialog-table" striped condensed bordered>
                  <thead>
                    <tr>
                      <th>Nutrient</th>
                      <th>Quantity</th>
                    </tr>
                  </thead>
                  <tbody>
                    { totalnutrients }
                  </tbody>
                </Table>
              </Col>
              <div className="clear"></div>
            </Tab>
            <Tab className="recipe-tab" label="Daily Nutrition" value="b">
              <Col sm={12} className="nopadding">
                <Table className="recipe-dialog-table" striped condensed bordered>
                  <thead>
                    <tr>
                      <th>Nutrient</th>
                      <th>Value</th>
                    </tr>
                  </thead>
                  <tbody>
                    { dailynutrients }
                  </tbody>
                </Table>
              </Col>
              <div className="clear"></div>
            </Tab>
          </Tabs>
        </Row>

      </div>
    );
  }
}

class RecipeCardComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openview: false
    };
  }

  handleOpen = () => {
    console.log('RecipeCardComponent::handleOpen');
    this.setState({openview: true});
  };
  handleClose = () => {
    console.log('RecipeCardComponent::handleClose');
    this.setState({openview: false});
  };
  handleOpenLink(url){
    console.log('RecipeCardComponent::handleOpenLink v:'+url);
    window.open(url);
  };

  updateSearchAction = (searchText) => {
    console.log('RecipeCardComponent::updateSearchAction v:'+searchText);
    this.props.updateSearchAction(searchText);
  }

  render() {
    const actions = [
      <FlatButton
        label="Open Recipe"
        primary={false}
        onTouchTap={ this.handleOpenLink.bind(null, this.props.datasrc.url) }
      />,
      <FlatButton
        label="Close"
        primary={true}
        onTouchTap={ this.handleClose }
      />
    ];
    var divStyle = {
      backgroundImage: 'url(' + this.props.datasrc.image + ')'
    }
    const customContentStyle = {
      width: '95%',
      maxWidth: '800px',
    };
    return (
      <Col sm={4} className="recipe-card-component">

      <Card onExpandChange={this.handleOpen}>
        <CardHeader
          title={this.props.title}
          subtitle={this.props.subtitle}
          className="recipe-card-header"
          actAsExpander={true}
        />
        <CardMedia className="recipe-card-component-overlay" overlay={<CardTitle title={this.props.title} subtitle={this.props.textdesc}/>} >
          <div className="recipe-card-img" style={ divStyle }> </div>
        </CardMedia>
      </Card>

      <Dialog
        className="recipe-dialog"
        contentStyle={customContentStyle}
        title={<div className="card-dialog-header">{this.props.title} <Subheader>{this.props.subtitle}</Subheader></div>}
        actions={actions}
        modal={false}
        open={this.state.openview}
        onRequestClose={this.handleClose}
        autoScrollBodyContent={true}
      >
        <BuildRecipeDialog className="recipe-dialog" datasrc={this.props.datasrc} updateSearchAction={ this.updateSearchAction }/>
      </Dialog>

      <div className="clear30"></div>

      </Col>
    );
  }
}

class RecipeResultsComponent extends Component {
  constructor(props) {
    super(props);
  }

  updateSearchAction = (searchText) => {
    console.log('RecipeResultsComponent::updateSearchAction v:'+searchText);
    this.props.updateSearchAction(searchText);
  }

  render() {
    var rows = [];
    this.props.datasource.forEach((recipe) => {
      rows.push(<RecipeCardComponent updateSearchAction={this.updateSearchAction} title={recipe.recipe.label} subtitle={ "By "+recipe.recipe.source} textdesc={recipe.recipe.source} imgsrc={recipe.recipe.image} key={recipe.recipe.uri} datasrc={recipe.recipe} />);
    });
    return (
      <div className="recipeResultsContainer">
        <Col xs={6}><h4 className="pull-left"> Search Results for '{this.props.searchText}' ({this.props.searchCount})</h4></Col>
        <Col xs={6}><h4 className="pull-right"> {(this.props.fromCount)?this.props.fromCount:1} to {(this.props.fromCount)?((this.props.fromCount+paginateItems>=this.props.searchCount)?this.props.searchCount:this.props.fromCount+paginateItems):((this.props.fromCount+paginateItems>=this.props.searchCount)?this.props.searchCount:paginateItems)}</h4></Col>
        <div className="clear20"></div>
        { rows }
      </div>
    );
  }
}

class SearchComponent extends Component {
  constructor(props) {
    super(props);
    this.handleSearchTextInput = this.handleSearchTextInput.bind(this);
    this.handleSearchClick = this.handleSearchClick.bind(this);
  }

  componentDidMount() {
    console.log('SearchComponent::componentDidMount');
  }

  handleSearchTextInput = (e) => {
    this.searchText = e.target.value;
    this.props.onSearchTextInput(e.target.value);
  }
  handleSearchClick = () => {
    console.log("SearchComponent::handleSearchClick")
    this.props.onSearchClick();
  }
  _handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.handleSearchClick();
    }
  }
  render() {
    this.searchText = this.props.setSearchText;

    var searchBox = (this.searchText)?
    <TextField
      hintText="Enter any ingredients"
      floatingLabelText="Search Recipes"
      fullWidth={true}
      value={ this.searchText }
      onChange={ this.handleSearchTextInput }
      onKeyPress={this._handleKeyPress}
    />:
    <TextField
      hintText="Enter any ingredients"
      floatingLabelText="Search Recipes"
      fullWidth={true}
      onChange={ this.handleSearchTextInput }
      onKeyPress={this._handleKeyPress}
    />;

    return (
      <div className="searchContainer">
        { searchBox }
        <RaisedButton 
          label="Search" 
          onClick={ this.handleSearchClick }
        />
      </div>
    );
  }
}

class LoadingComponent extends Component {
  render() {
    return (this.props.doShow)?
      <Col sm={12} className="text-center">
        <CircularProgress />
        <div className="clear20"></div>
      </Col>
    :null;
  }
}

class PageControlComponent extends Component {
  constructor(props) {
    super(props);
  }

  handleNextPage = () => {
    console.log("PageControlComponent::handleNextPage")
    this.props.onNextPageClick();
  }
  handlePrevPage = () => {
    console.log("PageControlComponent::handlePrevPage");
    this.props.onPrevPageClick();
  }

  render() {
    var pdisabled = (!this.props.isLoading && this.props.fromCount!=0)?false:true;
    var ndisabled = (!this.props.isLoading && this.props.isMore)?false:true;

    return (!pdisabled || !ndisabled)?
      <Col sm={12} className="text-center">
        <div className="clear20"></div>
        <FlatButton label="Previous Page" onClick={this.handlePrevPage} disabled={pdisabled} />
        <FlatButton label="Next Page" onClick={this.handleNextPage} disabled={ndisabled} />
        <div className="clear20"></div>
      </Col>
      :null;
  }
}

class SearchRecipeController extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
      isLoading: false,
      from: 0,
      searchCount: -1,
      more: false,
      datasource: [],
      hasErrored: false
    };
    this.searchText = ''; // static used for search results

    this.handleSearchTextInput = this.handleSearchTextInput.bind(this);
    this.handleSearchClick = this.handleSearchClick.bind(this);
    this.handleSearchAction = this.handleSearchAction.bind(this);
    this.handleNextPage = this.handleNextPage.bind(this);
    this.handlePrevPage = this.handlePrevPage.bind(this);
  }

  handleSearchTextInput(searchText){
    this.setState({ searchText: searchText });
  }
  handleSearchClick(){
    console.log("SearchRecipeController::handleSearchClick");
    var urlBuild = baseurl+'q='+this.state.searchText+'&to='+paginateItems;
    this.setState({ datasource: [] });
    this.searchText = this.state.searchText;
    this.fetchData(urlBuild);
  }
  handleSearchAction(searchText){
    console.log("SearchRecipeController::handleSearchAction v:"+searchText);
    var urlBuild = baseurl+'q='+searchText+'&to='+paginateItems;
    this.setState({ searchText: searchText, datasource: [] });
    this.searchText = searchText;
    this.fetchData(urlBuild);
  }
  handleNextPage(){
    console.log("SearchRecipeController::handleNextPage");
    var nextcount = this.state.from+paginateItems;
    var urlBuild = baseurl+'q='+this.searchText+"&from="+nextcount+'&to='+(nextcount+paginateItems);
    this.setState({ datasource: [] });
    this.fetchData(urlBuild);
  }
  handlePrevPage(){
    console.log("SearchRecipeController::handlePrevPage");
    var prevcount = this.state.from-paginateItems;
    var urlBuild = baseurl+'q='+this.searchText+"&from="+prevcount+'&to='+(prevcount+paginateItems);
    this.setState({ datasource: [] });
    this.fetchData(urlBuild);
  }

  fetchData(url) {
    console.log("SearchRecipeController::fetchData url:"+url);
    this.setState({ isLoading: true });

    fetch(url)
      .then((response) => {
          if (!response.ok)
            throw Error(response.statusText);

          this.setState({ isLoading: false });
          return response;
      })
      .then((response) => response.json())
      .then((datasource) => {
        this.setState({ 
          from: datasource.from,
          searchCount: datasource.count,
          more: datasource.more,
          datasource: datasource.hits
        });
        console.log("Raw: ");
        console.log(datasource);
        console.log("Results: "+datasource.count);
      })
      .catch(() => {
        this.setState({ 
          isLoading: false,
          hasErrored: true
        })
      });
  }
  
  render() {
    var showresult = null;

    if(this.state.searchCount>0 && !this.state.isLoading)
      showresult = <div><RecipeResultsComponent fromCount={ this.state.from } searchCount={ this.state.searchCount } searchText={ this.searchText } datasource={ this.state.datasource } updateSearchAction={ this.handleSearchAction }/></div>
    else if(this.state.searchCount==0 && !this.state.isLoading)
      showresult = <h4 className="text-center"> No Results for '{this.searchText}'</h4>
    else if(!this.state.isLoading && this.state.hasErrored)
      showresult = <h4 className="text-center"> Error Fetching Results :(</h4>

    return (
      <Grid>

        <Row>
          <Col sm={12}>
            <SearchComponent
              setSearchText={ this.state.searchText }
              onSearchTextInput={ this.handleSearchTextInput }
              onSearchClick={ this.handleSearchClick }
            />
          </Col>
        </Row>

        <div className="clear30"></div>

        <Row>
          <LoadingComponent doShow={this.state.isLoading}/>
          { showresult }
        </Row>

        <Row>
          <PageControlComponent isLoading={this.state.isLoading} isMore={this.state.more} fromCount={this.state.from} onNextPageClick={this.handleNextPage} onPrevPageClick={this.handlePrevPage}/>
        </Row>

      </Grid>
    );
  }
}

export default class App extends Component {
  render() {
    const iconStyle = { top: 4 };
    return (
      <div className="app-container">
        <AppBar title="Recipe Test App" />
        <SearchRecipeController />
      </div>
    );
  }
}